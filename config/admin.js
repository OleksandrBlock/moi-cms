module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '4be07f1e65306b6f6a752ff395aaed93'),
  },
});
