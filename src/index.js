'use strict';

const mysql = require('mysql');
const fastGlob = require('fast-glob');
const { parse } = require('path');
const { readFile } = require('fs/promises');

async function setupLocales(strapi) {
  const localeService = strapi.query('plugin::i18n.locale');
  const locales = [
    {
      name: 'Ukrainian (uk)',
      code: 'uk',
    },
  ];

  for (const locale of locales) {
    const existingLocale = await localeService.findMany({
      filters: {
        code: locale.code,
      },
      limit: 1,
    });
    if (existingLocale.length) {
      continue;
    }

    await localeService.create({
      data: locale,
    });
  }
}

async function setupAdmin(strapi) {
  const adminService = strapi.admin.services.user;
  const roleService = strapi.admin.services.role;

  if (await adminService.exists({ email: process.env.DEFAULT_ADMIN_EMAIL })) {
    return;
  }

  const adminRole = await roleService.findOne({
    code: roleService.constants.superAdminCode,
  });

  const result = await adminService.create({
    email: process.env.DEFAULT_ADMIN_EMAIL,
    username: process.env.DEFAULT_ADMIN_USERNAME,
    firstname: process.env.DEFAULT_ADMIN_USERNAME,
    lastname: process.env.DEFAULT_ADMIN_USERNAME,
    password: process.env.DEFAULT_ADMIN_PASSWORD,
    registrationToken: null,
    roles: [adminRole.id],
    isActive: 1,
  });

  console.log(`Admin user create `, result);
}

async function setupToken(strapi) {
  const TOKEN_NAME = 'dev-token';
  const tokenService = strapi.admin.services['api-token'];

  if (await tokenService.exists({ name: TOKEN_NAME })) {
    return console.log('Token exists');
  }

  await tokenService.create({
    name: TOKEN_NAME,
    type: 'read-only',
    access_key: process.env.DEFAULT_TOKEN,
  });

  console.log('Created API token');
}

async function setupContent(strapi) {
  // const seedFiles = await ;
  for (const seedFile of await fastGlob(['./database/seed/*.json'])) {
    const { name } = parse(seedFile);
    const data = JSON.parse(await readFile(seedFile, { encoding: 'utf-8' }));

    await Promise.all(
      data.map(async item => {
        const serviceKey = `api::${name}.${name}`;
        if (await strapi.entityService.findOne(serviceKey, item.id, {})) {
          return Promise.resolve(true);
        }

        return strapi.entityService.create(serviceKey, {
          data: item,
        });
      }),
    );
  }

  console.log('Data seeded!');
}

module.exports = {
  /**
   * An asynchronous register function that runs before
   * your application is initialized.
   *
   * This gives you an opportunity to extend code.
   */
  register: async ({ strapi }) => {
    const connection = mysql.createConnection({
      host: process.env.DATABASE_HOST,
      user: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
    });
    connection.connect();
    await new Promise(resolve => {
      connection.query(
        `CREATE DATABASE IF NOT EXISTS \`${process.env.DATABASE_NAME}\` CHARACTER SET = 'utf8' COLLATE = 'utf8_general_ci';`,
        () => resolve(true),
      );
    });
    connection.end();
  },

  /**
   * An asynchronous bootstrap function that runs before
   * your application gets started.
   *
   * This gives you an opportunity to set up your data model,
   * run jobs, or perform some special logic.
   */
  bootstrap: async ({ strapi }) => {
    if (process.env.NODE_ENV !== 'development') {
      return;
    }

    await setupAdmin(strapi);
    await setupToken(strapi);
    await setupLocales(strapi);
    await setupContent(strapi);
  },
};
